const http = require('http');
const fs = require('fs');
const webdriver = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const chromedriver = require('chromedriver');
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const path = require('path');
const AdmZip = require('adm-zip');



app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));



app.post('/mjerenje', async (req,res) => {
  if(req.body.mjerenje1 != ""){
    fs.unlink('mjerenje.json', function (err) {
  var data = JSON.parse(req.body.mjerenje1);
  fs.writeFile('mjerenje.json', JSON.stringify(data), function (err) {
    if (err) throw err;
    res.status(200).end();
  });
});
}
else{
  fs.unlink('mjerenje.json', function (err) {
fs.writeFile('mjerenje.json', "prazno", function (err) {
  if (err) throw err;
  res.status(200).end();
});
  });
}
});

//download file
app.get('/', (req, res) => {
  var uploadDir = fs.readdirSync(__dirname+"/files");
  const zip = new AdmZip();

  for(var i = 0; i < uploadDir.length;i++){
      zip.addLocalFile(__dirname+"/files/"+uploadDir[i]);
  }

  // Define zip file name
  const downloadName = `${Date.now()}.zip`;

  const data = zip.toBuffer();

  // save file zip in root directory
  zip.writeZip(__dirname+"/"+downloadName);
  
  // code to download zip file
  res.set('Content-Type','application/octet-stream');
  res.set('Content-Disposition',`attachment; filename=${downloadName}`);
  res.set('Content-Length',data.length);
  res.send(data);

})

app.get('/pocetna.html',function(req,res){
  res.sendFile(path.join(__dirname+'/pocetna.html'));
});
app.get('/pocetna.css',function(req,res){
  res.sendFile(path.join(__dirname+'/pocetna.css'));
});
app.get('/ucitavanje.js',function(req,res){
  res.sendFile(path.join(__dirname+'/ucitavanje.js'));
});
app.get('/background1.jpg',function(req,res){
  res.sendFile(path.join(__dirname+'/background1.jpg'));
});


app.get('/data/:url/:browser/:trajanje/:ucestalost/:sirina/:visina', (req,res) => {
loadPage("https://" + req.params.url, req.params.browser, req.params.trajanje, req.params.ucestalost, req.params.sirina, req.params.visina).then(value =>{
  
fs.readFile('mjerenje.json', (err, data) => {
      if(err) throw err;
     
      var dataFromFile = data.toString('utf8');

      if(dataFromFile != "prazno"){
      var json1 = JSON.parse(dataFromFile);  //uneseno mjerenje
      var json2 = JSON.parse(value);         //trenutne promjene

      //poređenje promjena

      var stranica1 = json1['Stranica'];
      var stranica2 = json2['Stranica'];

      var browser1 = json1['Browser'];
      var browser2 = json2['Browser'];


      var velicina1 = json1['VeličinaEkrana'];
      var velicina2 = json2['VeličinaEkrana']; 


      var datumivrijeme1 = json1['DatumIVrijeme'];
      var datumivrijeme2 = json2['DatumIVrijeme'];

      var promjene1 = json1['PROMJENE'];
      var promjene2 = json2['PROMJENE'];
   
      var zajednickePromjene = [];
      var unesenePromjene = [];
      var novePromjene = [];
      var pomocniNiz = [];
      for(let i=0; i<promjene1.length; i++) {
        let prviJSON = promjene1[i];
        let postoji = 0;
        for(let j = 0; j<promjene2.length; j++) {
           let drugiJSON = promjene2[j];
           if(prviJSON['Promjena']==drugiJSON['Promjena']){
              let prvoVrijeme = prviJSON['Vrijeme'];
              let drugoVrijeme = drugiJSON['Vrijeme'];

              let prvo = prvoVrijeme.split(' ');
              let drugo = drugoVrijeme.split(' ');
              let razlika = parseInt(prvo[0])-parseInt(drugo[0]);
              let zaJson = prviJSON['Promjena'] + " " + razlika + "ms";
              let promjena = {Promjena: zaJson};
              zajednickePromjene.push(promjena);
              postoji = 1;
              pomocniNiz.push(j);
              break;
           }    
        }
        if(postoji==0) unesenePromjene.push(promjene1[i]);
      }

      for(let k = 0; k < promjene2.length; k++) {
        if(!pomocniNiz.includes(k)){
          novePromjene.push(promjene2[k]);
        }
      }

      var ucitano = createJSONForResponse(stranica1, browser1, datumivrijeme1, velicina1, unesenePromjene);
      var tren = createJSONForResponse(stranica2, browser2, datumivrijeme2, velicina2, novePromjene);
      var zajenicko = {
        PROMJENE: zajednickePromjene
      };
      let vrati = {
        "Zajedničke promjene": zajenicko,
        "Trenutno mjerenje": tren,
        "Uneseno mjerenje": ucitano
      };
      let vrati1 = JSON.stringify(vrati);
      res.status(200).json(vrati1);
      res.end();
    }
    else {
      res.status(200).json(value);
      res.end();
    }
       
  });


});
});

app.listen(3000);


async function loadPage(url, browser, trajanjeP, ucestalostP, sirina, visina) {   
  let driver = await new webdriver.Builder()
      .forBrowser(browser)
      .build();
  try {
      //prvo ucitavanje
      await driver.manage().window().setRect({ width: parseInt(sirina), height: parseInt(visina) });
      await driver.get(url);
      let source1 = await driver.getPageSource();
      let dom1 = new JSDOM(source1);
      var elements1 = dom1.window.document.getElementsByTagName("*");

      let trajanje = parseInt(trajanjeP);
      let ucestalost = parseInt(ucestalostP);       
      let brojac = ucestalost;
      
      driver.sleep(ucestalost);

      var jsonTemplate = createJSONForFile(url,browser,sirina,visina);
      var forJsonFile = JSON.stringify(jsonTemplate);

      while(brojac<=trajanje){
          await driver.navigate().refresh();
          let source2 = await driver.getPageSource();
          let dom2 = new JSDOM(source2);
          var elements2 = dom2.window.document.getElementsByTagName("*");
          
          let rezultat = detectChanges(elements1,elements2, brojac);
          var obj = JSON.parse(forJsonFile);
          for(let i = 0; i < rezultat.length; i++) {
            obj['PROMJENE'].push(rezultat[i]);
          }
          forJsonFile = JSON.stringify(obj);
          elements1 = elements2;
           driver.sleep(ucestalost);
           brojac+=ucestalost;
      }

      fs.unlink('files/changes.json', function (err) {
        fs.writeFile('files/changes.json', forJsonFile, function (err) {
          if (err) return console.log(err);
       });
      });
      
      //await driver.close();
      await driver.get('http://localhost:3000/');
      return forJsonFile;
  }
  finally{

  }
}



function detectChanges(elements1,elements2, timestamp) {
  var tags1 = [];
  var tags2 = [];
   for (var i = 0; i < elements1.length; i++) {
     tags1.push(elements1[i].tagName);
     var hash = getHash(elements1[i]);
     elements1[i].setAttribute('hash', hash);
 }
 
 for (var i = 0; i < elements2.length; i++) {
   tags2.push(elements2[i].tagName);
   var hash = getHash(elements2[i]);
     elements2[i].setAttribute('hash', hash);
 }
   
 var m = new Array(tags1.length + 1);
 
 for (var i=0; i<m.length; i++){
   m[i] = new Array(tags2.length + 1);
   
   for (var j=0; j<m[i].length; j++){
     if (i === 0) m[i][j] = j;
     if (j === 0) m[i][j] = i;
   }
 }
 
 for (var i=1; i<=tags1.length; i++){
   for (var j=1; j<=tags2.length; j++){
 
     // no change needed
     if (tags1[i - 1] === tags2[j - 1]){
       m[i][j] = m[i - 1][j - 1];
   
     // choose deletion or insertion
     } else {
       m[i][j] = Math.min(m[i - 1][j], m[i][j - 1], m[i - 1][j - 1]) + 1;
     }
   }
 }
 
 //console.log('a: ' + JSON.stringify(a));
 //console.log('b: ' + JSON.stringify(b));
 
 var i = tags1.length,
     j = tags2.length,
     steps = [],
     elementsWithOperacionAndHash = [];
     
 while (i !== 0 && j !== 0){
   if (tags1[i - 1] === tags2[j - 1]){
    // steps = 'no change; ' + steps;
     i--;
     j--;
     
   } else if(m[i - 1][j] < m[i][j - 1]){
    var id = elements1[i-1].id;
    var klasa = elements1[i-1].className;
    var idAndClass = "";
    if(id != ''){
         idAndClass +=' [id = ' + '\'' + id + '\'' + ']';
    }
    if(klasa != ''){
     idAndClass +=' [class = ' + '\'' + klasa + '\'' + ']';
    }
 
    var root ='';
    var element = elements1[i-1];
    var parentData;
    var parent = element.parentElement;
    var pom = [];
    while(parent!=null) {
         parentData = '<'+ parent.nodeName;
           if(parent.id != ''){
             parentData += ' [id=\'' + parent.id + '\']'; 
           }
           if(parent.className != '') {
             parentData += ' [class=\'' + parent.className + '\']'; 
           }
         parentData+='>'; 
         pom.push(parentData);
         element = parent;
         parent = element.parentElement;
       }
        
 
       pom.reverse();
        for(let s = 0; s<pom.length; s++){
           root+=pom[s];
         }
       elements1[i-1].setAttribute('operacija', 'DELETE');
       elements1[i-1].setAttribute('pozicija', root);
       elementsWithOperacionAndHash.push(elements1[i-1]);
       steps.push(createJSON(timestamp,'Delete <' + tags1[i - 1] + '>' + idAndClass + ' at ' + root));
       i--;
     
   } else if (m[i - 1][j] === m[i][j - 1]){
       var id1 = elements1[i-1].id;
       var klasa1 = elements1[i-1].className;
       var data1 = "";
       if(id1 != ''){
         data1+=' [id = ' + '\'' + id1 + '\'' + ']';
       }
       if(klasa1 != ''){
         data1+=' [class = ' + '\'' + klasa1 + '\'' + ']';
       }
       var id2 = elements2[j-1].id;
       var klasa2 = elements2[j-1].className;
       var data2 = "";
       if(id2 != ''){
         data2+=' [id = ' + '\'' +id2 + '\'' + ']';
       }
       if(klasa2!=''){
         data2 += ' [class = ' + '\'' + klasa2 + '\'' + ']';
       }
 
       var root1 ='';
       var element1 = elements1[i-1];
       var parentData1;
       var parent1 = element1.parentElement;
       var pom1 = [];
       while(parent1!=null) {
             parentData1 = '<'+ parent1.nodeName;
             if(parent1.id != ''){
               parentData1 += ' [id=\'' + parent1.id + '\']'; 
             }
             if(parent1.className != '') {
               parentData1 += ' [class=\'' + parent1.className + '\']'; 
             }
             parentData1 += '>'; 
             pom1.push(parentData1);
             element1 = parent1;
             parent1 = element1.parentElement;
           }
       
 
       pom1.reverse();
       for(let s = 0; s<pom1.length; s++){
         root1+=pom1[s];
       }
 
       var root2 ='';
       var element2 = elements2[j-1];
       var parentData2;
       var parent2 = element2.parentElement;
       var pom2 = [];
       while(parent2!=null) {
           parentData2 = '<'+ parent2.nodeName;
           if(parent2.id != ''){
              parentData2 += ' [id=\'' + parent2.id + '\']'; 
          }
          if(parent2.className != '') {
            parentData2 += ' [class=\'' + parent2.className + '\']'; 
           }
         parentData2 += '>'; 
         pom2.push(parentData2);
         element2 = parent2;
         parent2 = element2.parentElement;
       }
 
       pom2.reverse();
       for(let s = 0; s<pom2.length; s++){
         root2+=pom2[s];
       }
       elements1[i-1].setAttribute('operacija', 'DELETE');
       elements1[i-1].setAttribute('pozicija', root1);
       elementsWithOperacionAndHash.push(elements1[i-1]);
 
       elements2[j-1].setAttribute('operacija', 'INSERT');
       elements2[j-1].setAttribute('pozicija', root2);
       elementsWithOperacionAndHash.push(elements2[j-1]);
 
       steps.push(createJSON(timestamp,'Delete <' + tags1[i - 1] + '>' + data1 + ' at ' + root1));
       steps.push(createJSON(timestamp,'Insert <' + tags2[j - 1] + '>' + data2 + ' at ' + root2));
       //steps.push('replace \'' + a[i - 1] + '\' with \'' + b[j - 1] + '\'; ');
       i--;
       j--;
     
   } else {
       var id = elements2[j-1].id;
       var klasa = elements2[j-1].className;
       var idAndClass = "";
       if(id!=''){
         idAndClass+=' [id = ' + '\'' +id + '\'' + ']';
       }
       if(klasa!=''){
         idAndClass+=' [class = ' + '\'' + klasa + '\'' + ']';
       } 
     
       var root ='';
       var element = elements2[j-1];
       var parentData;
       var parent = element.parentElement;
       var pom = [];
       while(parent!=null) {
           parentData = '<'+ parent.nodeName;
           if(parent.id != ''){
           parentData += ' [id=\'' + parent.id + '\']'; 
           }
           if(parent.className != '') {
           parentData += ' [class=\'' + parent.className + '\']'; 
           }
           parentData += '>'; 
           pom.push(parentData);
           element = parent;
           parent = element.parentElement;
         }
 
  
 
       pom.reverse();
       for(let s = 0; s<pom.length; s++){
          root+=pom[s];
       }
 
       elements2[j-1].setAttribute('operacija', 'INSERT');
       elements2[j-1].setAttribute('pozicija', root);
       elementsWithOperacionAndHash.push(elements1[i-1]);
       steps.push(createJSON(timestamp,'Insert <' + tags2[j - 1] + '>' + idAndClass + ' at ' +  root));
       j--;
   }
 }
 
       if (i === 0 && j > 0){
           for(let k = 0; k<j; k++){
              var id = elements2[k].id;
              var klasa = elements2[k].className;
              var idAndClass = "";
              if(id!=''){
                 idAndClass+=' [id = ' + '\'' +id + '\'' + ']';
              }
              if(klasa!=''){
                 idAndClass+=' [class = ' + '\'' + klasa + '\'' + ']';
              } 
     
       var root ='';
       var element = elements2[k];
       var parentData;
       var parent = element.parentElement;
       var pom = [];
       while(parent!=null) {
            parentData = '<'+ parent.nodeName;
            if(parent.id != ''){
               parentData += ' [id=\'' + parent.id + '\']'; 
            }
            if(parent.className != '') {
               parentData += ' [class=\'' + parent.className + '\']'; 
            }
            parentData +='>';
            pom.push(parentData);
            element = parent;
            parent = element.parentElement;
       }
        
  
       pom.reverse();
       for(let s = 0; s<pom.length; s++){
          root+=pom[s];
       }
 
       elements2[k].setAttribute('operacija', 'INSERT');
       elements2[k].setAttribute('pozicija', root);
       elementsWithOperacionAndHash.push(elements2[k]);
       steps.push(createJSON(timestamp,'Insert <' + tags2[k] + '>' + idAndClass + ' at ' + root));
     }
   
 } else if (j === 0 && i > 0){
       for(let k=0; k<i; k++){
         var id = elements1[k].id;
         var klasa = elements1[k].className;
         var idAndClass = "";
         if(id!=''){
            idAndClass+=' [id = ' + '\'' +id + '\'' + ']';
         }
         if(klasa!=''){
            idAndClass+=' [class = ' + '\'' + klasa + '\'' + ']';
         }
 
       var root ='';
       var element = elements1[k];
       var parentData;
       var parent = element.parentElement;
       var pom = [];
       while(parent!=null) {
            parentData = '<'+ parent.nodeName;
            if(parent.id != ''){
               parentData += ' [id=' + parent.id + ']'; 
            }
            if(parent.className != '') {
               parentData += ' [class=' + parent.className + ']'; 
             }
          parentData +='>';
          pom.push(parentData);
          element = parent;
          parent = element.parentElement;
       }
        
 
       pom.reverse();
       for(let s = 0; s<pom.length; s++){
          root+=pom[s];
       }
 
       elements1[k].setAttribute('operacija', 'DELETE');
       elements1[k].setAttribute('pozicija', root);
       elementsWithOperacionAndHash.push(elements1[k]);
 
       steps.push(createJSON(timestamp,'Delete <' + tags1[k] + '>' + idAndClass + ' at ' + root));
     }
 }
 
 
 for(let i = 0; i<elementsWithOperacionAndHash.length; i++) {
   if(elementsWithOperacionAndHash[i].getAttribute('operacija')=='DELETE'){
     for(let j = 0; j<elementsWithOperacionAndHash.length; j++) {
       if(elementsWithOperacionAndHash[j].getAttribute('operacija') == 'INSERT' && elementsWithOperacionAndHash[j].getAttribute('hash')==elementsWithOperacionAndHash[i].getAttribute('hash')) {
         var novi = createJSON(timestamp,'Move <' + elementsWithOperacionAndHash[i].tagName + '> from ' + elementsWithOperacionAndHash[i].getAttribute('pozicija') + ' to ' + elementsWithOperacionAndHash[j].getAttribute('pozicija'));
         steps[j] = novi;
         elementsWithOperacionAndHash[i].setAttribute('brisanje', 'obrisati');
       }
     }
   }
 }
 let deleted = 0;
 for(let i = 0; i<elementsWithOperacionAndHash.length; i++) {
   if(elementsWithOperacionAndHash[i].getAttribute('brisanje')=='obrisati') {
     steps.slice(i-deleted,1);
     deleted++;
   }
 }
 
 return(steps);
 };
 
 
 function createJSON(time, change) {
   return {
     Vrijeme: time + ' ms',
     Promjena: change
   };
 }

 function createJSONForFile(url, browser, sirina, visina) {
  var today = new Date();
     var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
     var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
     var dateTime = date+' '+time;
   return {
     Stranica: url,
     Browser: browser,
     DatumIVrijeme:  dateTime,
     VeličinaEkrana: sirina + ',' + visina,
     PROMJENE: []
   };
}


function createJSONForResponse(url, browser, datumivrijeme, sirinaIvisina, promjene){
  return {
    Stranica: url,
    Browser: browser,
    DatumIVrijeme: datumivrijeme,
    VeličinaEkrana: sirinaIvisina,
    PROMJENE: promjene
  };
}

 
 function getHash(element){
   let attributeNames = element.getAttributeNames();
   var hash = element.innerHTML;
   for(let i=0; i<attributeNames.length; i++){
       hash += element.getAttribute(attributeNames[i])
   }
   var p = 31;
   var m = 1e9 + 9;
   var power_of_p = 1;
   var hash_val = 0;
  
     // Loop to calculate the hash value
     // by iterating over the elements of string
   for (let i = 0; i < hash.length; i++) {
       hash_val = (hash_val + (hash[i] - 'a' + 1) * power_of_p) % m;
       power_of_p = (power_of_p * p) % m;
   }
   
   return hash_val;
 }
 